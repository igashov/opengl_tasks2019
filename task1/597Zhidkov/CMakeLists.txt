add_definitions(-DGLM_ENABLE_EXPERIMENTAL)
set(SRC_FILES
        ${PROJECT_SOURCE_DIR}/common/Application.cpp
        ${PROJECT_SOURCE_DIR}/common/DebugOutput.cpp
        ${PROJECT_SOURCE_DIR}/common/Camera.cpp
        ${PROJECT_SOURCE_DIR}/common/Mesh.cpp
        ${PROJECT_SOURCE_DIR}/common/ShaderProgram.cpp
        TorusModel.cpp
        torus.cpp
        )

MAKE_OPENGL_TASK(597Zhidkov 1 "${SRC_FILES}")
#
#set(HEADER_FILES
#        ${PROJECT_SOURCE_DIR}/common/Application.hpp
#        ${PROJECT_SOURCE_DIR}/common/DebugOutput.h
#        ${PROJECT_SOURCE_DIR}/common/Camera.hpp
#        ${PROJECT_SOURCE_DIR}/common/Mesh.hpp
#        ${PROJECT_SOURCE_DIR}/common/ShaderProgram.hpp
#        TorusModel.hpp
#        )
#
#set(SHADER_FILES
#        597ZhidkovData1/shader.vert
#        597ZhidkovData1/shader.frag
#        )
#
#source_group("Shaders" FILES
#        ${SHADER_FILES}
#        )
#
#MAKE_SAMPLE(torus)
#
#COPY_RESOURCE(597ZhidkovData1)
#add_dependencies(torus 597ZhidkovData1)
#
#
#install(DIRECTORY ${PROJECT_SOURCE_DIR}/nik2/597ZhidkovData1 DESTINATION ${CMAKE_INSTALL_PREFIX})


