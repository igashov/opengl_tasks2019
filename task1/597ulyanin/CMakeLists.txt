cmake_minimum_required(VERSION 3.0)

project(GraphicsSamples VERSION 3.0)

enable_language(CXX)
set(CMAKE_CXX_STANDARD 17)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG "${CMAKE_BINARY_DIR}/bin")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG "${CMAKE_BINARY_DIR}/bin")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG "${CMAKE_BINARY_DIR}/lib")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE "${CMAKE_BINARY_DIR}/bin")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE "${CMAKE_BINARY_DIR}/bin")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE "${CMAKE_BINARY_DIR}/lib")

set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O2")

set_property(GLOBAL PROPERTY USE_FOLDERS ON)
set_property(GLOBAL PROPERTY PREDEFINED_TARGETS_FOLDER "CMake Targets")

set(CMAKE_DEBUG_POSTFIX "d")
set(CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake")

include(MacroUtils)
link_libraries(stdc++fs)

add_subdirectory(external)


add_subdirectory(heigths_map)

install(DIRECTORY ${PROJECT_SOURCE_DIR}/shaders/ DESTINATION ${CMAKE_INSTALL_PREFIX}/shaders)

# Здесь нужные библиотеки (dll) копируются из папки с зависимостями в папку install для примеров
if(WIN32)
    get_property(GLFW_LOCATION_DEBUG TARGET glfw PROPERTY IMPORTED_LOCATION_DEBUG)
    if(GLFW_LOCATION_DEBUG)
        install(FILES ${GLFW_LOCATION_DEBUG} DESTINATION ${CMAKE_INSTALL_PREFIX})
    else()
        message("GLFW_LOCATION_DEBUG is empty")
    endif()

    get_property(GLFW_LOCATION_RELEASE TARGET glfw PROPERTY IMPORTED_LOCATION_RELEASE)
    if(GLFW_LOCATION_RELEASE)
        install(FILES ${GLFW_LOCATION_RELEASE} DESTINATION ${CMAKE_INSTALL_PREFIX})
    else()
        message("GLFW_LOCATION_RELEASE is empty")
    endif()

    install(FILES ${DEPENDENCIES_ROOT}/bin/assimp-${ASSIMP_MSVC_VERSION}-mtd.dll DESTINATION ${CMAKE_INSTALL_PREFIX})
    install(FILES ${DEPENDENCIES_ROOT}/bin/assimp-${ASSIMP_MSVC_VERSION}-mt.dll DESTINATION ${CMAKE_INSTALL_PREFIX})
endif(WIN32)

############################################

file(GLOB STUDENTS students/*)
foreach(STUDENT ${STUDENTS})
    if(IS_DIRECTORY ${STUDENT})
        message("Student task found " ${STUDENT})
        add_subdirectory("${STUDENT}")
    endif()
endforeach()
